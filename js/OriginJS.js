/*
**	原生ADT相关处理库
*/
(function(){

/************	字符串String操作	*************/
/*	字符串重复 */
if(!String.repeat){
	String.prototype.repeat = function(times){
		return new Array(times+1).join(this);
	}
}

/* 
**	破折号转驼峰命名
**	eg:hello-man -> helloMan
*/
if(!String.camelize){
	String.prototype.camelize = function(){
		return this.replace(/-(\w)/g,function(strMatch,pl){
			return pl.toUpperCase();
		})
	}
}
})();