/*
**	利用“立即执行函数”及其函数体构建js命名空间
*/
(function(){
/*	创建SKY库，所有函数通过此字典的属性来调用*/
if(!window['SKY']){window['SKY']={};}

/*	通过一个或多个id查询元素，返回元素列表*/
function $(){
	var elements = [];
	for(var i =0;i<arguments.length;i++){
		var arg = arguments[i];
		if(typeof arg == 'string'){
			/*只有一个元素情况下返回元素对象而非数组*/
			var element = document.getElementById(arg);	
			if(arguments.length==1){return element;}

			elements.push(element);
		}
	}
	return elements;
}
window['SKY']['$'] = $;

/*
**	事件监听，node可通过SKY.$()等方式获取，只响应节点类型
**	可能移除注册事件时，传入的listener不能用匿名函数，如此removeEvent的句柄才能与其一致
*/
function addEvent(node,type,listener){
	if (node.addEventListener) {
		node.addEventListener(type,listener);
	}
}
window['SKY']['addEvent'] = addEvent;

/*
**	移除所监听事件
**	有需要调用此函数时，传入的listener也要与addEvent时的一致
*/
function removeEvent(node,type,listener){
	if(node.removeEventListener){
		node.removeEventListener(type,listener);
	}
}
window['SKY']['removeEvent'] = removeEvent;

/*
**	更改函数调用的执行环境，指定函数内this的指向——this指向与函数调用者不同时使用
*/
function bidFunction(obj,someFunc){
	return function(){
		someFunc.apply(obj,arguments);
	}
}
window['SKY']['bidFunction'] = bidFunction;

/*
**	遍历DOM树
**  @param func 		遍历出的每个节点所执行的函数
*/
function walkTheDomRecursive(node,depth,returnedFromParent,func){
	var beginNode = node||window.document;
	var returnedFromParent = func.call(beginNode,depth++,returnedFromParent);
	var node = beginNode.firstChild;
	while(node){
		walkTheDomRecursive(node,depth,returnedFromParent,func);
		node = node.nextSibling;
	}
}
window['SKY']['walkTheDomRecursive'] = walkTheDomRecursive;


})()/*end of function body!*/